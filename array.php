<?php
//for loop indexed array
$langs = ['JS','CSS','Angular','React','Bootstrap'];
echo count($langs) . "</br>";
$langs[] = "HTML";
echo count($langs) . "</br>";
echo "<ul>";
foreach($langs as $lang ){
    echo "<li>" . $lang . "</li>" ;
}

//Associative array
echo "Associative array" . "<br>";
$myLangs = ['Html'=>'70%','Css'=>'80%','JS'=>'60%','Angular'=>'50%'];
echo "<ul>";
foreach($myLangs as $myLang => $progress){
    echo "<li>" . $myLang . " => " . $progress . "</li>";
}
echo "</ul>";

//  Metho Array

if(in_array(10 , $myLangs)){
    echo "Yes is Exist";
}
//method  array_search
$array = array_search('CSS' , $langs);
echo "Its found " . $array . " is Value :" . $langs[$array];