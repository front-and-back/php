<?php

//function more dynamic
function generateYears($start , $end){
    echo "<select name='year'>";
    for($year = $start ; $year <= $end ; $year++){
        echo "<option>" . $year . "</option>";
    }
    echo "</select>" . "<br>";
}
generateYears(1990 , 2021);

// function calculate age
function calculateAge($age){
    return $age * 360 ;
}

echo calculateAge(20) . "<br>";

//function say hello
function sayHello($name){
    return "Hello " . $name . " How are you ?";
}
echo sayHello('Rafeef') . "<br>";

function calculateSalaryInDay($name , $days){
    return $name . " your salary " . $days * 100;
}

echo calculateSalaryInDay("Rafeef",22) . "<br>";

//return vs echo
$view = 150 ;
function  incressView($view){
    echo  $view + 1 ;
}
var_dump(incressView($view));